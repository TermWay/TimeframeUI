﻿/// The MIT License (MIT) Copyright (c) 2017 Gabriel Le Breton, 2019 Termway etst

using UnityEditor;
using UnityEngine;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

static class BuildCommand
{
    [MenuItem("Build/Windows/x64")]
    static void BuildWindows()
    {
        Debug.Log("> Performing Windows64 build.");
        Debug.Log("> Scenes in build " + EditorBuildSettings.scenes.ToFlattenString() + ", count " + UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings);
        Debug.Log("> Scenes in project " + FindAllScenes().ToFlattenString() + ", count " + FindAllScenes().Length);

        string buildDirectory = Path.Combine(Path.Combine(Path.Combine(Application.dataPath, ".."), "Build"), "Windowsx64");
        DirectoryInfo directoryInfo = new DirectoryInfo(buildDirectory);
        Debug.Log(">> Directory " + directoryInfo.FullName);

        if (!directoryInfo.Parent.Exists)
        {
            directoryInfo.Parent.Create();
            Debug.Log(">> Create directory " + directoryInfo.FullName);
        }
        else
            directoryInfo.Parent.Delete(true);

        Console.WriteLine("> levels " + ScourScenesToBuild().ToFlattenString() + " buildPath " + buildDirectory + ".exe");


        BuildCompatibility(
            ScourScenesToBuild(),
            buildDirectory + ".exe",
            BuildTarget.StandaloneWindows64,
            BuildOptions.ShowBuiltPlayer);
    }

    static void PerformBuild()
    {
        Console.WriteLine(">> Performing build");

        BuildTarget buildTarget = GetBuildTarget();
        string buildPath = GetBuildPath();
        string buildName = GetBuildName();
        string fixedBuildPath = GetFixedBuildPath(buildTarget, buildPath, buildName);
        Console.WriteLine("> Scenes in build " + EditorBuildSettings.scenes.ToFlattenString() + ", count " + UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings);
        Console.WriteLine("> Scenes in project " + FindAllScenes().ToFlattenString() + ", count " + FindAllScenes().Length);
        Console.WriteLine("> levels " + ScourScenesToBuild().ToFlattenString() + " buildPath " + buildPath + " buildName " + buildName + " fixedBuiledPath " + fixedBuildPath);

        BuildCompatibility(ScourScenesToBuild(), fixedBuildPath, buildTarget, GetBuildOptions());
    }

    static void BuildCompatibility(string[] levels, string locationPathName, BuildTarget target, BuildOptions options)
    {
#if UNITY_2018_1_OR_NEWER
        UnityEditor.Build.Reporting.BuildReport buildReport = BuildPipeline.BuildPlayer(levels, locationPathName, target, options);
        Console.WriteLine(">> Build " + buildReport.summary.result);
        Console.WriteLine(">> Creating " + buildReport.files.Length + " files " + buildReport.summary.ToPropertiesString());
        Console.WriteLine(">> Is built file exist ? " + File.Exists(buildReport.summary.outputPath));
#else
        string buildReport = BuildPipeline.BuildPlayer(levels, locationPathName, target, options);
        Console.WriteLine(">> Build report " + buildReport);
#endif
    }

    static BuildTarget GetBuildTarget()
    {
        string buildTargetName = GetArgument("customBuildTarget");
        Console.WriteLine(">> Received customBuildTarget " + buildTargetName);

        if (buildTargetName.ToLower() == "android")
        {
#if !UNITY_5_6_OR_NEWER
			// https://issuetracker.unity3d.com/issues/buildoptions-dot-acceptexternalmodificationstoplayer-causes-unityexception-unknown-project-type-0
			// Fixed in Unity 5.6.0
			// side effect to fix android build system:
			EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;
#endif
        }

        return ToEnum<BuildTarget>(buildTargetName, BuildTarget.NoTarget);
    }

    static string GetBuildPath()
    {
        string buildPath = GetArgument("customBuildPath");
        Console.WriteLine(">> Received customBuildPath " + buildPath);
        if (buildPath == "")
        {
            throw new Exception("customBuildPath argument is missing");
        }
        return buildPath;
    }

    static string GetBuildName()
    {
        string buildName = GetArgument("customBuildName");
        Console.WriteLine(">> Received customBuildName " + buildName);
        if (buildName == "")
            throw new Exception("customBuildName argument is missing");

        return buildName;
    }

    static string GetFixedBuildPath(BuildTarget buildTarget, string buildPath, string buildName)
    {
        if (buildTarget.ToString().ToLower().Contains("windows"))
            buildName = buildName + ".exe";
        return buildPath + buildName;
    }

    static BuildOptions GetBuildOptions()
    {
        string buildOptions = GetArgument("customBuildOptions");
        return buildOptions == "AcceptExternalModificationsToPlayer" ? BuildOptions.AcceptExternalModificationsToPlayer : BuildOptions.None;
    }

    static string[] ScourScenesToBuild()
    {
        string[] scenes = GetEditorBuildScenes(true);
        if (scenes.Length > 0)
            return scenes;

        scenes = GetEditorBuildScenes(false);
        if (scenes.Length > 0)
            return scenes;

        return FindAllScenes();
    }


    static string[] FindAllScenes()
    {
        string[] guids = AssetDatabase.FindAssets("t:scene");
        return guids.Select(guid => AssetDatabase.GUIDToAssetPath(guid)).ToArray();
    }

    static string[] GetEditorBuildScenes(bool enabledScene)
    {
        return (from scene in EditorBuildSettings.scenes
            where scene.enabled == enabledScene
            where !string.IsNullOrEmpty(scene.path)
            select scene.path).ToArray();
    }


    public static string GetArgument(string name)
    {
        string[] args = Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
            if (args[i].Contains(name))
                return args[i + 1];
        return null;
    }

    // https://stackoverflow.com/questions/1082532/how-to-tryparse-for-enum-value
    static TEnum ToEnum<TEnum>(this string strEnumValue, TEnum defaultValue)
    {
        if (!Enum.IsDefined(typeof(TEnum), strEnumValue))
        {
            return defaultValue;
        }

        return (TEnum)Enum.Parse(typeof(TEnum), strEnumValue);
    }

    static string getEnv(string key, bool secret = false, bool verbose = true)
    {
        string env_var = Environment.GetEnvironmentVariable(key);
        if (verbose)
        {
            if (env_var != null)
            {
                if (secret)
                {
                    Console.WriteLine(">> env['" + key + "'] set");
                }
                else
                {
                    Console.WriteLine(">> env['" + key + "'] set to '" + env_var + "'");
                }
            }
            else
            {
                Console.WriteLine(">> env['" + key + "'] is null");
            }
        }
        return env_var;
    }

    public static string ToPropertiesString(this object obj, string seperator = " : ")
    {
        PropertyInfo[] propertyInfos = obj.GetType().GetProperties();
        StringBuilder sb = new StringBuilder();
        foreach (PropertyInfo propertyInfo in propertyInfos)
            sb.Append(propertyInfo.Name).
               Append(seperator).
               AppendLine(propertyInfo.GetValue(obj, null).ToString());

        return sb.ToString();
    }

    public static string ToFlattenString<T>(this IEnumerable<T> enumerable, string separator = ";")
    {
        string[] elements = enumerable.Select(e => e.ToString()).ToArray();
        return string.Join(separator, elements);
    }
}
