﻿#if UNITY_EDITOR
namespace Termway.TimeframeUI
{
    using Termway.Helper;

    using System;
    using System.Linq;
    using System.Reflection;

    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(TimeframeUI))]
    [CanEditMultipleObjects]
    public class TimeframeEditor : Editor
    {
        TimeframeUI timeframeUI;
        SerializedPropertyManager pm;

        SerializedProperty toggleUiKey;
        SerializedProperty defaultVisibility;
        SerializedProperty statsFrameRange;
        SerializedProperty targetFramerate;
        SerializedProperty uiAnchorPosition;
        SerializedProperty uiOffset;

        SerializedProperty gridUpdateDelaySeconds;
        SerializedProperty showLegend;
        SerializedProperty showMs;
        SerializedProperty showFps;
        SerializedProperty gridCellSize;
        SerializedProperty gridRowsData;

        SerializedProperty showGraph;
        SerializedProperty graphWidthAutoResize;
        SerializedProperty graphWidth;
        SerializedProperty graphTextureSize;
        SerializedProperty graphMsUpperLimit;
        SerializedProperty graphColorAlternateTint;
        SerializedProperty test;

        void OnEnable()
        {
            timeframeUI = (TimeframeUI)target;

            toggleUiKey = serializedObject.FindProperty(timeframeUI._ToggleUiKeyName);
            defaultVisibility = serializedObject.FindProperty(timeframeUI._DefaultVisibilityName);
            statsFrameRange = serializedObject.FindProperty(timeframeUI._StatsFrameRangeName);
            targetFramerate = serializedObject.FindProperty(timeframeUI._TargetFramerateName);
            uiAnchorPosition = serializedObject.FindProperty(timeframeUI._UiAnchorPositionName);
            uiOffset = serializedObject.FindProperty(timeframeUI._UiOffsetName);

            gridUpdateDelaySeconds = serializedObject.FindProperty(timeframeUI._GridUpdateDelaySecondsName);
            showLegend = serializedObject.FindProperty(timeframeUI._ShowLegendName);
            showMs = serializedObject.FindProperty(timeframeUI._ShowMsName);
            showFps = serializedObject.FindProperty(timeframeUI._ShowFpsName);
            gridCellSize = serializedObject.FindProperty(timeframeUI._GridCellSizeName);
            gridRowsData = serializedObject.FindProperty(timeframeUI._GridRowsDataName);

            showGraph = serializedObject.FindProperty(timeframeUI._ShowGraphName);
            graphWidthAutoResize = serializedObject.FindProperty(timeframeUI._GraphWidthAutoResizeName);
            graphWidth = serializedObject.FindProperty(timeframeUI._GraphWidthName);
            graphTextureSize = serializedObject.FindProperty(timeframeUI._GraphTextureSizeName);
            graphMsUpperLimit = serializedObject.FindProperty(timeframeUI._GraphMsUpperLimitName);
            graphColorAlternateTint = serializedObject.FindProperty(timeframeUI._GraphColorAlternateTintName);

            Action buildUiAction = () => timeframeUI.BuildUI();

            pm = new SerializedPropertyManager(serializedObject);
            pm.Add(toggleUiKey);
            pm.Add(defaultVisibility, () => timeframeUI.SetUiState(defaultVisibility.boolValue));
            pm.Add(statsFrameRange, () => timeframeUI.InitStats());

            FieldInfo targetFramerateField = timeframeUI.GetType().GetField(timeframeUI._TargetFramerateName, BindingFlags.Instance | BindingFlags.NonPublic);
            TooltipAttribute[] tooltips = targetFramerateField.GetCustomAttributes(typeof(TooltipAttribute), true) as TooltipAttribute[];

            pm.Add(targetFramerate,
                () => Application.targetFrameRate = (int) timeframeUI.TargetFramerate,
                () => targetFramerate.intValue = EditorGUILayout.DelayedIntField(new GUIContent("Target Framerate", tooltips != null && tooltips.Length > 0 ? tooltips[0].tooltip : ""), targetFramerate.intValue));
            pm.Add(uiAnchorPosition, buildUiAction);
            pm.Add(uiOffset, buildUiAction);

            pm.Add(gridUpdateDelaySeconds, "Update Delay (s)", () => timeframeUI.CorrectGridUpdateDelay());
            pm.Add(showLegend, buildUiAction);
            pm.Add(showMs, buildUiAction);
            pm.Add(showFps, buildUiAction);
            pm.Add(gridCellSize, () => timeframeUI.ResizeGridUI());
            pm.Add(gridRowsData, buildUiAction);

            pm.Add(showGraph, buildUiAction);
            pm.Add(graphWidthAutoResize, buildUiAction);
            pm.Add(graphWidth, () => timeframeUI.ResizeGraphUI());
            pm.Add(graphTextureSize, () => timeframeUI.RecreateTexture());
            pm.Add(graphMsUpperLimit, () => timeframeUI.UpdateAllTextureColumns());
            pm.Add(graphColorAlternateTint, () => timeframeUI.RecreateTexture());
        }
    
        public override void OnInspectorGUI()
        {
            pm.Do();
        }  
    }
}
#endif
