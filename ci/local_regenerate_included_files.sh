#!/usr/bin/env bash

dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)

CI_PROJECT_NAME=timeframeui
GENERATE_NAME=TimeframeUI

. $dir/../citemplateunity3d/ci/concat_functions.sh
cut_file_begin_end $dir/../README.md $dir/../$CI_PROJECT_NAME/Assets/$GENERATE_NAME/README.md "# $GENERATE_NAME" "# License"

cp  $dir/../LICENSE.md $dir/../$CI_PROJECT_NAME/Assets/$GENERATE_NAME/LICENSE.md

