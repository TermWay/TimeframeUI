# CI Status
[![pipeline status](https://gitlab.com/TermWay/TimeframeUI/badges/master/pipeline.svg)](https://gitlab.com/TermWay/TimeframeUI/commits/master)

# All-in-one script file
One MonoBehaviour in one concatenated file:

- [C# file](https://termway.gitlab.io/TimeframeUI/TimeframeUI.cs)
- [Zip file](https://gitlab.com/TermWay/TimeframeUI/-/jobs/artifacts/master/download?job=generate_allinone_script_file)
- [Snippet page](https://gitlab.com/TermWay/TimeframeUI/snippets/1904028)

# Unity package
The unitypackage file contains a sample scene and scripts in a more organized way. It also contains a zip with the all-in-script.

- [Unitypackage file](https://termway.gitlab.io/timeframeui/TimeframeUI.unitypackage)
- [Zip file](https://gitlab.com/TermWay/TimeframeUI/-/jobs/artifacts/master/download?job=export_unitypackage)

# WebGL demo
A WebGL demo is available on the following link:  

- [WebGL demo page](https://termway.gitlab.io/TimeframeUI/)
- [Zip file](https://gitlab.com/TermWay/TimeframeUI/-/jobs/artifacts/master/download?job=build_WebGL_Unity_2018)

# TimeframeUI
A simple text and graph UI to monitor performance in Unity3D.

# Gitlab project page
The source code is available on the gitlab page project: https://gitlab.com/TermWay/TimeframeUI

# Asset store page
https://assetstore.unity.com/packages/tools/gui/timeframeui-156810

# Unity forum page
https://forum.unity.com/threads/timeframeui-a-simple-ui-to-monitor-performance-open-source.795063/

# Supported Unity3D versions
This asset has been tested on Unity 5.6, 2017.4 LTS, 2018.4 LTS and 2019.2.

# Quick overview video
A short video showcasing this asset is available online: https://youtu.be/ssvVOzirybI 

# Quick setup
There are multiple ways to add this UI to your Unity3D project.

## Add the TimeframeUI component
Add the TimeframeUI component or drag and drop the TimeframeUI.cs file on a GameObject.

## Create from the Unity3D menu
From the menu GameObject > UI > Timeframe, it will create a new GameObject in the hierarchy.

# Features

## Global features
*  Toggle UI Key: short key to toggle the UI visibility. Visibility does not stop the recording of the time frame values.
*  Default visibility of the UI which is the visibility on the editor and the visibility on the first frame of the application.
*  Stats Frame Range: latest frame number used to compute statistics on.
*  Target Framerate: force the frame rate value. The zero value means that there is no forced frame rate.
*  UI Anchor Position: anchor to snap the UI at one corner:
   *  (0,0) is bottom left,
   *  (0,1) is bottom right,
   *  (1,0) is top left,
   *  (1,1) is top right.
    Will rebuild the UI.
*  UI Offset: UI absolute global offset in pixel according to the anchor position. Will rebuild the UI.

## Grid features
Displays the latest statistics values in a text grid.

*  Update delay: delay in second between each value update of the grid.
*  Show legend: visibility of the legend row and column. Will rebuild the UI.
*  Show ms: visibility of the time frame (millisecond) column. Will rebuild the UI.
*  Show fps: visibility of the frame rate (frame per second) column. Will rebuild the UI.
*  Grid Cell Size: cell size of the grid in pixel. Will resize the grid. Will rebuild the UI.
*  Grid Rows Data: statistics to include in the grid display in the given order. Each data is a row in the grid:
   *  Stats Type to be computed among min, max, current, average and percentile.
      -  Average: compute the average from the last recorded values specified. The zero value force all recorded values to be used.
      -  Current: compute the average from the last recorded values specified. The zero value force the current time frame to be used.
      -  Min: get the minimal time frame recorded.
      -  Max: get the maximal time frame recorded.
      -  Percentile: compute the percentile given value of all recorded values.
   *  Value: statistics parameter.
   *  Graph Representation: point (line) or bar (histogram) rendering. None disables the graph representation.
   *  Apply Color To Text: if the color is also applied to the grid.
   *  Color:  color used on the graph for this statistic.
   *  Color Operation: how color is painted over previous statistics on the graph.
      Will rebuild the UI.

## Graph features
Displays the latest statistics values in a histogram graph.

*  Show graph: visibility of the graph. Will rebuild the UI.
*  Graph Auto Resize: automatic resize of the width when the resolution changed. Will rebuild the graph.
*  Graph Width: width of the graph. If the auto resize option is activated and the value set to zero the graph will take all the available width space. Will rebuild the graph.
*  Graph Texture Size: texture size of the graph. Each column on the X axis has a unique statistic value. Should be below Stats Frame Range value. Will recreate the graph texture. 
*  Graph Ms Upper Limit: upper limit of Y axis of the graph in millisecond. The zero value means it's computed as twice the current median. Will recreate the graph texture.
*  Graph Color Alternate Tint: tint color of the graph used to contrast new override painted value. Tint is multiplied to all the color painted.

Manual configuration can also be done once the UI is generated. For example the background color and texts style. Some feature will rebuild the UI and manual modification will be lost in that case.

# All-in-one script file
An all-in-one file is also included in the TimeframeUI.cs.zip file. To use it, remove every C# scripts of this asset and unzip the all-in-one TimeframeUI.cs file.

# UI generation
TimeframeUI used the Unity3D native UI to render all information. Individual UI.Text for each value and a UI.Image to display the graph via a texture updated each frame with different values.

## Hierarchy 
There are three mains objects:  
*  A screen space canvas to display the UI and served as a parent GameObject for the two following element.
*  A container for the grid information which has all statistics in different texts components.
*  An image to display the graph thanks to a texture where a column is updated each frame.

```
TimeFrameUI (TimeframeUI)
├─ Canvas (UI.Canvas)
|  ├─ GridImage (UI.Image, UI.GridLayoutGroup)
|  |  ├─  Legend_Legend (UI.Text)
|  |  ├─  Legend_Stats_1  (UI.Text)
|  |  ├─  ...
|  |  ├─  Legend_Stats_N  (UI.Text)

|  |  ├─  Ms_Ms  (UI.Text)
|  |  ├─  Ms_Stats_1  (UI.Text)
|  |  ├─  ...
|  |  ├─  Ms_Stats_N  (UI.Text)

|  |  ├─  Fps_Fps  (UI.Text)
|  |  ├─  Fps_Stats_1  (UI.Text)
|  |  ├─  ...
|  |  ├─  Fps_Stats_N  (UI.Text)
|  ├─ GraphImage (UI.Image)
```
There are variations in the GridImage GameObject depending on the current TimeframeUI configuration.

# Performances
The most important factor to improve performances is to reduce the size of the graph texture. A small texture (< 20k pixels, 256x64) takes less than a millisecond to render on most systems but a larger texture (> 100k pixels) takes longer. A very high Stats Frame Range value will also drop performances.

# License
[MIT License](LICENSE.md) (c) 2019 Termway.

## gitlab-ci
Some code used for the gitlab-ci come from @gableroux. More information can be found on his page project https://gitlab.com/gableroux/unity3d-gitlab-ci-example.


I used it to create a [ci template for Unity3D](https://gitlab.com/TermWay/CiTemplateUnity3D) which I used as submodule in this project.
